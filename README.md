Damn client
===========

`damn_client` is part of **Divide and map. Now.** -- damn project. The purpose
of the project is to help mappers by dividing some big area into smaller
squares that a human can map.

`damn_client` lets users contribute on `damn_server` instance.

License
-------

The project is published under [MIT License][].

[MIT License]: ./LICENSE

Contribute
==========

This project is [KISS][] damn client. Use only static files and no JavaScript
libraries.

Use [OneFlow][] branching model and keep the [changelog][].

Write [great git commit messages][]:

1. Separate subject from body with a blank line.
2. Limit the subject line to 50 characters.
3. Capitalize the subject line.
4. Do not end the subject line with a period.
5. Use the imperative mood in the subject line.
6. Wrap the body at 72 characters.
7. Use the body to explain what and why vs. how.

[KISS]: https://en.wikipedia.org/wiki/KISS_principle
[OneFlow]: https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow
[changelog]: ./CHANGELOG.md
[great git commit messages]: https://chris.beams.io/posts/git-commit/
