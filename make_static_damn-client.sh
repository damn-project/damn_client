#!/bin/bash
cat static/index.html \
    | sed 's,<link rel="stylesheet" href="def.css">,<style>\n</style>,' \
    | sed '/<style>/ r static/def.css' \
    | sed '/<script src="lib.js"><\/script>/d' \
    | sed '/<script src="i18n.js"><\/script>/d' \
    | sed '/<script src="f.js"><\/script>/d' \
    | sed '/<script>/ r static/f.js' \
    | sed '/<script>/ r static/i18n.js' \
    | sed '/<script>/ r static/lib.js' \
    > damn-client.html
