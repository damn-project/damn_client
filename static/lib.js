/*

The damn client library
=======================

These are basic functions for communication with some damn server instance. The
library is written with procedural programming paradigm in mind.

Set up global variables in client script
----------------------------------------

- DAMN_SERVER = "URL to running damn server instance."

API
---

All GET callbacks (cb) functions should *store* received object and *show* it
in HTML. You can see the damn server API docs [1].

If the GET or POST fail, the fail callback (fcb) is called. To maintain
backward compatibility, the fail callback is optional.

[1]: https://server.damn-project.org/docs

- get_areas(cb, fcb=false)
- get_area(cb, aid, fcb=false)
- get_commits(cb, aid, since, fcb=false)

- post_area_commit(cb, commit, fcb=false)
- post_square_commit(cb, commit, fcb=false)
- put_area(cb, area, fcb=false)

This library is published under MIT license
-------------------------------------------

Copyright (c) 2020 Jiri Vlasak

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/
// Private procedures
function ep(e) // Return damn server endpoint.
{
    return DAMN_SERVER+e;
}
function up(i, w) // Update div's innerHTML.
{
    document.getElementById(i).innerHTML = w;
}
function get_ajaj(cb, url, fcb=false) // Send GET AJAJ request.
{
    var http_request = new XMLHttpRequest();
    http_request.open("GET", url, true);
    http_request.responseType = "json";
    http_request.onreadystatechange = function () {
        var done = 4, ok = 200;
        if (http_request.readyState === done && http_request.status === ok) {
            cb(http_request.response);
        } else if (http_request.readyState === done && fcb !== false) {
            fcb(http_request.response);
        }
    };
    http_request.send(null);
}
function post_ajaj(cb, obj, url, fcb=false)
{
    var http_request = new XMLHttpRequest();
    http_request.open("POST", url, true);
    http_request.setRequestHeader("Content-Type", "application/json");
    http_request.setRequestHeader("Authorization", "Bearer " + load_token());
    http_request.responseType = "json";
    http_request.onreadystatechange = function() {
        var done = 4, ok = 201;
        if (http_request.readyState === done && http_request.status === ok) {
            cb(http_request.response);
        } else if (http_request.readyState === done && fcb !== false) {
            fcb(http_request.response);
        }
    };
    http_request.send(JSON.stringify(obj));
}
function put_ajaj(cb, obj, url, fcb=false)
{
    var http_request = new XMLHttpRequest();
    http_request.open("PUT", url, true);
    http_request.setRequestHeader("Content-Type", "application/json");
    http_request.setRequestHeader("Authorization", "Bearer " + load_token());
    http_request.responseType = "json";
    http_request.onreadystatechange = function() {
        var done = 4, ok = 200;
        if (http_request.readyState === done && http_request.status === ok) {
            cb(http_request.response);
        } else if (http_request.readyState === done && fcb !== false) {
            fcb(http_request.response);
        }
    };
    http_request.send(JSON.stringify(obj));
}
function delete_token()
{
    document.cookie = "token=;";
}
function load_token() // Load token from cookie.
{
    var token;
    try {
        var t = document.cookie.match(/token=(.*)/);
        token = RegExp.$1;
    } catch (e) {
        token = false;
    }
    return token;
}
function jwt_payload(token)
{
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var payload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' +
        c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
    return JSON.parse(payload);
}
function load_user()
{
    var u;
    try {
        var t = load_token();
        u = jwt_payload(t);
    } catch (e) {
        u = false;
    }
    return u;
}
function authenticate() // Authenticate to OpenStreetMap server.
{
    var chs = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var tid = "";
    for (var i = 0; i < 64; i++)
        tid += chs.charAt(Math.floor(Math.random() * chs.length));
    var child = window.open(DAMN_SERVER + "/auth/" + tid);
    var check = setInterval(function() {
        get_ajaj(
            function(obj) {
                if (!("token" in obj))
                    return;
                clearTimeout(check);
                c = "token=" + obj["token"];
                var d = new Date();
                d.setFullYear(d.getFullYear() + 1);
                c += "; expires=" + d.toUTCString();
                document.cookie = c;
                location.reload();
            },
            ep("/token/" + tid),
        );
    }, 100);
}

// API
function get_areas(cb, fcb=false)
{
    get_ajaj(cb, ep("/areas"), fcb);
}
function get_area(cb, aid, fcb=false)
{
    get_ajaj(cb, ep("/area/" + aid), fcb);
}
function get_commits(cb, aid, since, fcb=false)
{
    if (!isNaN(parseFloat(aid)) && isFinite(aid))
        get_ajaj(cb, ep("/area/" + aid + "/commits?since=" + since), fcb);
    else
        get_ajaj(cb, ep("/user/" + aid + "/commits?since=" + since), fcb);
}
function post_area_commit(cb, commit, fcb=false)
{
    post_ajaj(cb, commit, ep("/area/" + commit["aid"] + "/commits"), fcb);
}
function post_square_commit(cb, commit, fcb=false)
{
    post_ajaj(
        cb,
        commit,
        ep("/area/" + commit["aid"] + "/square/" + commit["sid"] + "/commits"),
        fcb,
    );
}
function put_area(cb, area, fcb=false) // Update area.
{
    put_ajaj(cb, area, ep("/area/" + area["aid"]), fcb);
}
